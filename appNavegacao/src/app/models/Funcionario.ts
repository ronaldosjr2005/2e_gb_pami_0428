import { Pessoa } from './Pessoa'

export class Funcionario extends Pessoa {
    data_admissao: string
    salario: number
    cargo: string
    setor: string
}

export class Livro{
    
    private _titulo : string;
    private _paginas : number;
    private _idioma : string;
    private _anoPublicacao: number;
    private _edicao : number;
    private _preco : number;
 
    constructor(titulo:string, paginas:number, idioma:string, anoPublicacao:number, edicao:number, preco:number){
        this._titulo = titulo;
        this._paginas = paginas;
        this._idioma = idioma;
        this._anoPublicacao = anoPublicacao;
        this._edicao = edicao;
        this._preco = preco;
    }
 
    public set titulo(titulo:string){
        this._titulo = titulo;
    }
 
    public get titulo() :string{
        return this._titulo;
    }
 
    public set paginas(paginas:number){
        this._paginas = paginas;
    }
 
    public get paginas() :number{
        return this._paginas;
    }
 
    public set idioma(idioma:string){
        this._idioma = idioma;
    }
 
    public get idioma() :string{
        return this._idioma;
    }
 
    public set anoPublicacao(anoPublicacao:number){
        this._anoPublicacao = anoPublicacao;
    }
 
    public get anoPublicacao() :number{
        return this._anoPublicacao;
    }
 
    public set edicao(edicao:number){
        this._edicao = edicao;
    }
 
    public get edicao() :number{
        return this._edicao;
    }
    
    public set preco(preco:number){
        this._preco = preco;
    }
 
    public get preco() :number{
        return this._preco;
    }  


}
import { Usuario } from './../../models/Usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_1 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL_2= "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL = this.URL_1
  constructor(
    private http: HttpClient
  ) { }

  logar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }

  buscarUsuarios(): Observable<any>{
    return this.http.get<any>(`${this.URL}usuarios`)
  }

  cadastrar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }
}

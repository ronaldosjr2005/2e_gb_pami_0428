import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from 'src/app/models/Funcionario';

@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {
  private readonly URL_1 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL_2 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL = this.URL_1

  constructor(
    private http: HttpClient
  ) { }

  buscarFuncionarios(): Observable<any>{
    return this.http.get<any>(`${this.URL}funcionarios`)
  } 
  
  cadastrar(funcionario:Funcionario): Observable<any> {
    return this.http.post<any>(`${this.URL}funcionario`, funcionario)
  }
}

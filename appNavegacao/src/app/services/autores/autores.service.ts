import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_1 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL_2= "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL = this.URL_1
  constructor(
    private http: HttpClient
  ) { }

  buscarAutores(): Observable<any>{
    return this.http.get<any>(`${this.URL}autores`)
  }

  cadastrar(autor: Autor): Observable <any> {
    return this.http.post<any>(`${this.URL}autor`, autor)
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Editora } from 'src/app/models/Editora';

@Injectable({
  providedIn: 'root'
})
export class EditorasService {
  private readonly URL_1 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL_2 = "https://3000-zombieritua-2egbapi0810-3tu77xwwiaz.ws-us78.gitpod.io/"
  private readonly URL = this.URL_1

  constructor(
    private http: HttpClient
  ) { }

  buscarEditoras(): Observable<any>{
    return this.http.get<any>(`${this.URL}editoras`)
  } 
  
  cadastrar(editora:Editora): Observable<any> {
    return this.http.post<any>(`${this.URL}editora`, editora)
  }

}

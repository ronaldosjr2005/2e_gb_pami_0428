import { Component, OnInit } from '@angular/core';
import { Autor } from '../models/Autor';
import { AutoresService } from '../services/autores/autores.service';
@Component({
  selector: 'app-cad-autor',
  templateUrl: './cad-autor.page.html',
  styleUrls: ['./cad-autor.page.scss'],
})
export class CadAutorPage implements OnInit {
  rotas = [
    {
      path: "/cad-usuario",
      text: "Cadastro de Usuario"
    },
    {
      path: "/cad-funcionario",
      text: "Cadastro de Funcionarios"
    },
    {
      path: "/cad-editora",
      text: "Cadastro de Editora"
    },
   {
     path:"/home",
     text:"Tela Home"
   }
    ]
private autor: Autor

constructor(
  private autoresService: AutoresService
) {
  this.autor = new Autor()
}
  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.autor)

    this.autoresService.cadastrar(this.autor)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}

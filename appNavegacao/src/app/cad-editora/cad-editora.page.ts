import { Component, OnInit } from '@angular/core';
import { Editora } from '../models/Editora';
import { EditorasService } from '../services/editoras/editoras.service';
@Component({
  selector: 'app-cad-editora',
  templateUrl: './cad-editora.page.html',
  styleUrls: ['./cad-editora.page.scss'],
})
export class CadEditoraPage implements OnInit {
  rotas = [
    {
      path: "/cad-usuario",
      text: "Cadastro de Usuario"
    },
    {
      path: "/cad-funcionario",
      text: "Cadastro de Funcionarios"
    },
    {
      path: "/cad-autor",
      text: "Cadastro Do Autor"
    },
   {
     path:"/home",
     text:"Tela Home"
   }
    ]
  private editora: Editora
  constructor(
    private editorasService : EditorasService
  ) { 
    this.editora = new Editora ()
  }

  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.editora)

    this.editorasService.cadastrar(this.editora)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

  
}

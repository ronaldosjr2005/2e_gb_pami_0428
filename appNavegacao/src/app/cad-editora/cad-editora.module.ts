import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadEditoraPageRoutingModule } from './cad-editora-routing.module';

import { CadEditoraPage } from './cad-editora.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadEditoraPageRoutingModule
  ],
  declarations: [CadEditoraPage]
})
export class CadEditoraPageModule {}

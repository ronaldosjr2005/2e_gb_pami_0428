import { UsuariosService } from './../../services/usuarios/usuarios.service';
import { Usuario } from './../../models/Usuario';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-adm-login',
  templateUrl: './adm-login.page.html',
  styleUrls: ['./adm-login.page.scss'],
})
export class AdmLoginPage implements OnInit {
  private user: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.user = new Usuario()
  }

  ngOnInit() {
  }

  logar (): void {
    console.log(this.user)

    this.usuariosService.logar(this.user)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}

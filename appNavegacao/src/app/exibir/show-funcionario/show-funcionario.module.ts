import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowFuncionarioPageRoutingModule } from './show-funcionario-routing.module';

import { ShowFuncionarioPage } from './show-funcionario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowFuncionarioPageRoutingModule
  ],
  declarations: [ShowFuncionarioPage]
})
export class ShowFuncionarioPageModule {}

import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/Funcionario';
import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';

@Component({
  selector: 'app-show-funcionario',
  templateUrl: './show-funcionario.page.html',
  styleUrls: ['./show-funcionario.page.scss'],
})
export class ShowFuncionarioPage implements OnInit {

  funcionarios: Funcionario[]
  constructor(
    private funcionariosService: FuncionariosService
  ) {
    this.funcionarios = []
   }


   ngOnInit(): void {
    this.funcionariosService.buscarFuncionarios().subscribe({
      next: (resposta) => {
        this.funcionarios = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
  }
}

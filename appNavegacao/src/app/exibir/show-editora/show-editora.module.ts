import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowEditoraPageRoutingModule } from './show-editora-routing.module';

import { ShowEditoraPage } from './show-editora.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowEditoraPageRoutingModule
  ],
  declarations: [ShowEditoraPage]
})
export class ShowEditoraPageModule {}

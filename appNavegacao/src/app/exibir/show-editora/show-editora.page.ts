import { Component, OnInit } from '@angular/core';
import { Editora } from 'src/app/models/Editora';
import { EditorasService } from 'src/app/services/editoras/editoras.service';

@Component({
  selector: 'app-show-editora',
  templateUrl: './show-editora.page.html',
  styleUrls: ['./show-editora.page.scss'],
})
export class ShowEditoraPage implements OnInit {

  editoras: Editora[]
  constructor(
    private editorasService: EditorasService
  ) {
    this.editoras = []
   }


   ngOnInit(): void {
    this.editorasService.buscarEditoras().subscribe({
      next: (resposta) => {
        this.editoras = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
  }

}

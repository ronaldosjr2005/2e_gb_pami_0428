import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { AutoresService } from 'src/app/services/autores/autores.service';

@Component({
  selector: 'app-show-autor',
  templateUrl: './show-autor.page.html',
  styleUrls: ['./show-autor.page.scss'],
})
export class ShowAutorPage implements OnInit {
  autores: Autor[]
  constructor(
    private autoresService: AutoresService
  ) {
    this.autores = []
   }


   ngOnInit(): void {
    this.autoresService.buscarAutores().subscribe({
      next: (resposta) => {
        this.autores = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
  }

}

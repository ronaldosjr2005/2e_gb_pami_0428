import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-show-usuario',
  templateUrl: './show-usuario.page.html',
  styleUrls: ['./show-usuario.page.scss'],
})
export class ShowUsuarioPage implements OnInit {
  usuarios: Usuario[]
  constructor(
    private usuariosService: UsuariosService
  ) {
    this.usuarios = []
   }


   ngOnInit(): void {
    this.usuariosService.buscarUsuarios().subscribe({
      next: (resposta) => {
        this.usuarios = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
  }

}
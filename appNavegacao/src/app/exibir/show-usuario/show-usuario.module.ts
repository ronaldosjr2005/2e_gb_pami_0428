import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowUsuarioPageRoutingModule } from './show-usuario-routing.module';

import { ShowUsuarioPage } from './show-usuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowUsuarioPageRoutingModule
  ],
  declarations: [ShowUsuarioPage]
})
export class ShowUsuarioPageModule {}

import { Component, OnInit } from '@angular/core';
import { Funcionario } from '../models/Funcionario';
import { FuncionariosService } from '../services/funcionarios/funcionarios.service';

@Component({
  selector: 'app-cad-funcionario',
  templateUrl: './cad-funcionario.page.html',
  styleUrls: ['./cad-funcionario.page.scss'],
})
export class CadFuncionarioPage implements OnInit {
  rotas = [
    {
      path: "/cad-usuario",
      text: "Cadastro de Usuario"
    },
    {
      path: "/cad-editora",
      text: "Cadastro de Editoras"
    },
    {
      path: "/cad-autor",
      text: "Cadastro Do Autor"
    },
   {
     path:"/home",
     text:"Tela Home"
   }
    ]
private funcionario : Funcionario
  constructor(
    private funcionariosService : FuncionariosService
  ) 
  {
    this.funcionario = new Funcionario ()
   }

  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.funcionario)

    this.funcionariosService.cadastrar(this.funcionario)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}

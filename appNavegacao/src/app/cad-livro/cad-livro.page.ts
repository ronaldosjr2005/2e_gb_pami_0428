import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cad-livro',
  templateUrl: './cad-livro.page.html',
  styleUrls: ['./cad-livro.page.scss'],
})
export class CadLivroPage implements OnInit {

  rotas = [
    {
      path: "/cad-autor",
      text: "Cadastro Do Autor"
    },
    {
      path: "/cad-editora",
      text: "Cadastro de Editora"
    },
   {
     path:"/home",
     text:"Tela Home"
   }
    ]

  constructor() { }

  ngOnInit() {
  }

}

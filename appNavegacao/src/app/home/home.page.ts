import { Component } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  fundo_foto="../../../assets/fundoApp.jpg"
  rotas = [
  {
    path: "/cad-autor",
    text: "Cadastro Do Autor"
  },
  {
    path: "/cad-editora",
    text: "Cadastro de Editora"
  },
  {
    path: "/cad-usuario",
    text: "Cadastro de Usuario"
  },
  {
    path: "/cad-funcionario",
    text: "Cadastro de Funcionarios"
  }

  ]
  constructor() {}

}

import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.page.html',
  styleUrls: ['./cad-usuario.page.scss'],
})
export class CadUsuarioPage implements OnInit {
  rotas = [
    {
      path: "/cad-editora",
      text: "Cadastro de Editora"
    },
    {
      path: "/cad-funcionario",
      text: "Cadastro de Funcionarios"
    },
    {
      path: "/cad-autor",
      text: "Cadastro Do Autor"
    },
   {
     path:"/home",
     text:"Tela Home"
   }
    ]
  private user: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.user = new Usuario()
  }

  ngOnInit() {
  }

  cadastrar (): void {
    console.log(this.user)

    this.usuariosService.cadastrar(this.user)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
   {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
 
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'detalhes',
    loadChildren: () => import('./detalhes/detalhes.module').then( m => m.DetalhesPageModule)
  },
  {
    path: 'cad-livro',
    loadChildren: () => import('./cad-livro/cad-livro.module').then( m => m.CadLivroPageModule)
  },
  {
    path: 'cad-autor',
    loadChildren: () => import('./cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('./cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'adm-login',
    loadChildren: () => import('./admin/adm-login/adm-login.module').then( m => m.AdmLoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./admin/usuarios/usuarios.module').then( m => m.UsuariosPageModule)
  },
  {
    path: 'adm-login',
    loadChildren: () => import('./admin/adm-login/adm-login.module').then( m => m.AdmLoginPageModule)
  },
  {
    path: 'cad-funcionario',
    loadChildren: () => import('./cad-funcionario/cad-funcionario.module').then( m => m.CadFuncionarioPageModule)
  },
  {
    path: 'show-usuario',
    loadChildren: () => import('./exibir/show-usuario/show-usuario.module').then( m => m.ShowUsuarioPageModule)
  },
  {
    path: 'show-autor',
    loadChildren: () => import('./exibir/show-autor/show-autor.module').then( m => m.ShowAutorPageModule)
  },
  {
    path: 'show-editora',
    loadChildren: () => import('./exibir/show-editora/show-editora.module').then( m => m.ShowEditoraPageModule)
  },
  {
    path: 'show-funcionario',
    loadChildren: () => import('./exibir/show-funcionario/show-funcionario.module').then( m => m.ShowFuncionarioPageModule)
  },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
